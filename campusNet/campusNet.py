import argparse
import time
import re
from urllib.parse import urlparse
from urllib.parse import quote
from urllib.parse import unquote
import urllib.request


redirectPortal = "http://123.123.123.123"
loginUrl = "http://10.100.1.5/eportal/InterFace.do?method=login"

USERID = ''
PASSWORD = ''
SERVICE = ''
#SERVICE = '中国移动'
#SERVICE = '中国联通'
#SERVICE = '中国电信'
#SERVICE = '校园网'

CAPTIVE = ['http://www.google.cn/generate_204','http://connectivitycheck.platform.hicloud.com/generate_204','http://www.apple.com/library/test/success.html','http://wifi.vivo.com.cn/generate_204','http://g.cn/generate_204']
KEYWORDS = ['Success','success','connect','Microsoft']

payload = "userId={0}&password={1}&service={2}&queryString={3}&operatorPwd=&operatorUserId=&validcode=&passwordEncrypt=false"
headers = {
  'Accept': '*/*',
  'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
  'Cache-Control': 'no-cache',
  'Connection': 'keep-alive',
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  'DNT': '1',
  'Pragma': 'no-cache',
  'Referer': 'http://10.100.1.5/eportal/index.jsp?',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 14_0) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15'
}
def net(s):
    if s == 'edu':
        return '校园网'
    if s == 'mobile':
        return '中国移动'
    if s == 'unicom':
        return '中国联通'
    if s == 'telecom':
        return '中国电信'
    print("No Valid Network For",s)
    return ""

def check():
    n = 0
    for i in CAPTIVE:
        print("########")
        print(i)
        req = urllib.request.Request(method="GET",url=i,headers=headers)
        resp = urllib.request.urlopen(req)
        if  resp.status in [200,204] and 'eportal' not in resp.read().decode("utf-8"):
            print(resp.status,resp.reason)
            print(resp.read().decode("utf-8"))
            n = n + 1

    print('Connected %d of %d' %(n,len(CAPTIVE)))
    if n > 2:
        print('I N T E R N E T // C O N N E C T E D !')
        return True
    else:
        print('I N T E R N E T // D I E D !')
        return False

def connect(username,password,service):
    payload = "userId={0}&password={1}&service={2}&queryString={3}&operatorPwd=&operatorUserId=&validcode=&passwordEncrypt=false"
    resp = urllib.request.urlopen(urllib.request.Request(method="GET",url=redirectPortal,headers=headers))
    redirectUrl = resp.read().decode("utf-8")
    print('########')
    print('Captive Response: ',redirectUrl)
    pattern = "\'([^\"]*)\'"
    redirectUrl = re.findall(pattern=pattern,string=redirectUrl)
    url = redirectUrl[0]
    print('########')
    print('Captive Login Url: ',url)
    queryString = urlparse(url).query
    payload = payload.format(username,password,service,quote(queryString))
    payload = str.encode(payload) 
    print("########")
    print("Current Service:",unquote(unquote(service)))
    print("Logining For",username,'...')
    resp = urllib.request.urlopen(urllib.request.Request(method="POST",url=loginUrl,data=payload,headers=headers))
    print(resp.status,resp.reason)
    respBody = resp.read().decode("utf-8")
    print(respBody)

def main():
    if check():
        return
    else:
        connect(USERID,PASSWORD,quote(quote(SERVICE)))
        check()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            prog='campusNet',
            description='Connect Campus Network Console Client. By 0tt0')
    if (USERID == '') or (PASSWORD == '') or (SERVICE == ''):
        parser.add_argument('-d',dest='daemon',action="store_true",help='Run Script As Daemon.')
        parser.add_argument('-u',dest='Username',type=str,required=True,help='Your Student ID.')
        parser.add_argument('-p',dest='Password',type=str,required=True,help='Your XMUNET+ PASSWORD. https://pass.xmu.edu.cn')
        parser.add_argument('-net',dest='Network',type=str,required=True,help='e.g. mobile,telecom,unicom,edu')
        parser.print_help()
        args = parser.parse_args()
        USERID=args.Username
        PASSWORD=args.Password
        SERVICE=net(args.Network)
    print(args.daemon)
    if args.daemon:
        while True:
            main()
            time.sleep(5)
    main()
 
